(open-dribble-file (expand-file-name (format-time-string "~/.emacs.d/.local/lossage-%Y-%m-%dT%H:%M.txt")))

(defvar ediff-do-hexl-diff nil
  "variable used to store trigger for doing diff in hexl-mode")
(defadvice ediff-files-internal (around ediff-files-internal-for-binary-files activate)
  "catch the condition when the binary files differ

the reason for catching the error out here (when re-thrown from the inner advice)
is to let the stack continue to unwind before we start the new diff
otherwise some code in the middle of the stack expects some output that
isn't there and triggers an error"
  (let ((file-A (ad-get-arg 0))
        (file-B (ad-get-arg 1))
        ediff-do-hexl-diff)
    (condition-case err
        (progn
          ad-do-it)
      (error
       (if ediff-do-hexl-diff
           (let ((buf-A (find-file-noselect file-A))
                 (buf-B (find-file-noselect file-B)))
             (with-current-buffer buf-A
               (hexl-mode 1))
             (with-current-buffer buf-B
               (hexl-mode 1))
             (ediff-buffers buf-A buf-B))
         (error (error-message-string err)))))))

(defadvice ediff-setup-diff-regions (around ediff-setup-diff-regions-for-binary-files activate)
  "when binary files differ, set the variable "
  (condition-case err
      (progn
        ad-do-it)
    (error
     (setq ediff-do-hexl-diff
           (and (string-match-p "^Errors in diff output.  Diff output is in.*"
                                (error-message-string err))
                (string-match-p "^\\(Binary \\)?[fF]iles .* and .* differ"
                                (buffer-substring-no-properties
                                 (line-beginning-position)
                                 (line-end-position)))
                (y-or-n-p "The binary files differ, look at the differences in hexl-mode? ")))
     (error (error-message-string err)))))

;; from rust-module help page
;; in $DOOMDIR/config.el
(setq rustic-lsp-server 'rust-analyzer)

(defun rustic-cargo-nightly-clippy ()
  "Run `cargo +nightly clippy`."
  (interactive)
  (let ((command (list rustic-cargo-bin "+nightly" "clippy"))
        (buf rustic-clippy-buffer-name)
        (proc rustic-clippy-process-name)
        (mode 'rustic-cargo-clippy-mode))
    (rustic-compilation-process-live)
    (rustic-compilation command
                        :buffer buf
                        :process proc
                        :mode mode)))

(after! rustic-cargo
  (defun rustic-cargo-nightly-clippy-fix ()
    "Run `cargo +nightly clippy --fix -Z unstable-options`."
    (interactive)
    (let ((command (list rustic-cargo-bin "+nightly" "clippy" "--fix" "-Z" "unstable-options"))
          (buf rustic-clippy-buffer-name)
          (proc rustic-clippy-process-name)
          (mode 'rustic-cargo-clippy-mode))
      (rustic-compilation-process-live)
      (rustic-compilation command
                          :buffer buf
                          :process proc
                          :mode mode))))

;; (map! "C-t" 'my-macro-rust-cargo-test-test-utils--! )
(setq rustic-test-arguments " --all-features")

(after! rustic-cargo
  (defun rustic-cargo-test (&optional arg)
    "Run 'cargo test'.

If ARG is not nil, use value as argument and store it in `rustic-test-arguments'.
When calling this function from `rustic-popup-mode', always use the value of
`rustic-test-arguments'."
    (interactive "P")
    (rustic-cargo-test-run
     (cond (arg (setq rustic-test-arguments
                      (read-from-minibuffer "Cargo test arguments: " rustic-test-arguments)))
           (t rustic-test-arguments)))))

(setq evil-want-minibuffer t)

(after! evil
  (map! :n "g b" #'evil-replace-with-register))

(fset 'one-line--functions--for--later--sort
   (kmacro-lambda-form [?/ ?f ?u ?n ?c ?. ?* ?\( return ?v ?i ?f ?k ?$ ?: ?s ?/ ?\\ ?n ?/ ?* backspace ?@ ?@ ?@ ?@ return ?w] 0 "%d"))

;; (fset 'my-macro-rust-remove-dbg-macro
;;    [kp-divide ?d ?b ?g ?! return ?d ?f ?! ?% ?a ?_ ?\; ?_ escape ?F ?\) ?d ?s ?\) kp-divide ?_ ?\; ?_ return ?3 ?x])
(eval(fset 'my-macro-rust-remove-todo-macro--!
           (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([kp-divide ?t ?o ?d ?o ?! return ?" ?_ ?d ?f ?! ?d ?s ?\(] 0 "%d")) arg))))

;; (fset 'my-macro-rust-remove-dbg-macro
;;    [kp-divide ?d ?b ?g ?! return ?d ?f ?! ?% ?a ?_ ?\; ?_ escape ?F ?\) ?d ?s ?\) kp-divide ?_ ?\; ?_ return ?3 ?x])
(eval(fset 'my-macro-rust-remove-dbg-macro--!
      (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([kp-divide ?d ?b ?g ?! return ?" ?_ ?d ?f ?! ?d ?s ?\(] 0 "%d")) arg))))

(eval(fset 'my-macro-rust-wrap-statement-in-dbg-macro--!
           [?i ?d ?b ?g ?! escape escape ?l ?y ?s ?t ?\; ?\( ?f ?\;]))
(fset 'my-macro-rust-visual-wrap-with-dbg!--!
      [?S ?\( ?i ?d ?b ?g ?! escape ])

(fset 'append-gud-enum-str-type
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([121 105 111 109 109 63 112 105 backspace 117 98 32 S-insert return 122 122 63 116 110 115 95 111 112 116 105 111 110 115 32 116 121 112 101 114 backspace 61 return 102 61 108 121 101 96 109 36 104 105 44 32 escape 112 104 108 121 97 105 111 escape 100 100 107 36 104 104 121 115 105 111 34 106 94 119] 0 "%d")) arg)))

(eval (fset 'my-macro-cpp-remove-dbg-calls--!
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([kp-divide 100 98 103 40 return 100 119 100 115 40] 0 "%d")) arg))))

;; (fset 'my-macro-go-to-path-using-projectile-file-finder-p-f--!
;;    [?y ?t ?: ?  ?w ?h ?  ?p ?f S-insert return ?\C-x ?o ?j ?  ?w ?h])
;; A")ed the goto line number ex:42
(fset 'my-macro-go-to-path-using-projectile-file-finder-p-f--!
   [?y ?t ?: ?  ?w ?h ?  ?p ?f S-insert return ?  ?w ?l ?f ?: ?l ?y ?t ?: ?B ?j ?  ?w ?h ?: S-insert return])

(fset 'my-macro-go-to-file-line-from-bottom-of-return-delimited-list-using-SPC-p-f--!
   [?y ?t ?: ?  ?w ?h ?  ?p ?f S-insert return ?  ?w ?l ?f ?: ?l ?y ?t ?: ?B ?k ?  ?w ?h ?: S-insert return])

(fset 'extract-run-clause-of-yaml-github-workflow-to-right-window
   (kmacro-lambda-form [kp-divide ?r ?u ?n ?: return down ?V kp-divide ?\\ ?n ?\\ ?n return up down ?y ?  ?w ?l ?p ?G ?  ?w ?h] 0 "%d"))

(require 'keyfreq)
(setq keyfreq-excluded-commands
      '(self-insert-command
        abort-recursive-edit
        forward-char
        backward-char
        previous-line
        next-line
        evil-forward-char
        evil-backward-char
        evil-previous-line
        evil-next-line))
(keyfreq-mode 1)
(keyfreq-autosave-mode 1)

(add-to-list 'company-backends #'company-tabnine)

(map! :map grep-mode-map
      (:prefix "C-c"
       :g "C-e" 'wgrep-change-to-wgrep-mode))
;; For debuging purposes, print full map of grep
;; (print grep-mode-map)

;; (define-key ctl-x-map "\C-c" 'save-buffers-kill-terminal)
(define-key ctl-x-map "\C-c" 'nil)

;; Preview files in dired
;; From http://pragmaticemacs.com/emacs/quickly-preview-images-and-other-files-with-peep-dired/
(use-package peep-dired
  :ensure t
  :defer t ; don't access `dired-mode-map' until `peep-dired' is loaded
  :bind (:map dired-mode-map
         ("P" . peep-dired)))
;; CUSTOMIZE (from https://github.com/duchainer/peep-dired)
;; Or you can choose to kill the buffer just after you move to another entry in the dired buffer.

(setq peep-dired-cleanup-eagerly t)
;; If you want the dired buffers that were peeped to have the mode enabled set it to true.

(setq peep-dired-enable-on-directories t)
;; Evil integration
;; Adjust the state name depending on an evil state you open dired in:

(evil-define-key 'normal peep-dired-mode-map (kbd "<SPC>") 'peep-dired-scroll-page-down
  (kbd "C-<SPC>") 'peep-dired-scroll-page-up
  (kbd "<backspace>") 'peep-dired-scroll-page-up
  (kbd "j") 'peep-dired-next-file
  (kbd "k") 'peep-dired-prev-file)
(add-hook 'peep-dired-hook 'evil-normalize-keymaps)

;; Gives a non-nil, non-t value to display no-break spaces as escaped space
(setq nobreak-char-display 0)

(setq projectile-project-search-path '("~/Documents/Documents_240gb/NestingSafe/repos"))

(add-to-list 'default-frame-alist '(fullscreen . maximized))

(atomic-chrome-start-server)

(setq hl-todo-keyword-faces
'(
  ("TODO" warning bold)
  ("FIXME" error bold)
  ("HACK" font-lock-constant-face bold)
  ("REVIEW" font-lock-keyword-face bold)
  ("NOTE" success bold)
  ("DEPRECATED" font-lock-doc-face bold)
 )
)

(nconc hl-todo-keyword-faces
   '(("HOLD" . "#d0bf8f")
    ;; ("TODO" . "#cc9393")
    ("NEXT" . "#dca3a3")
    ("THEM" . "#dc8cc3")
    ("PROG" . "#7cb8bb")
    ("OKAY" . "#7cb8bb")
    ("DONT" . "#5f7f5f")
    ("FAIL" . "#8c5353")
    ("DONE" . "#afd8af")
    ;; ("NOTE"   . "#d0bf8f")
    ("KLUDGE" . "#d0bf8f")
    ;; ("HACK"   . "#d0bf8f")
    ("TEMP"   . "#d0bf8f")
    ;; ("FIXME"  . "#cc9393")
    ("XXX+"   . "#cc9393")) )

(defun my-compilation-mode-hook ()
  (setq truncate-lines nil) ;; automatically becomes buffer local
  (set (make-local-variable 'truncate-partial-width-windows) nil))
(add-hook 'compilation-mode-hook 'my-compilation-mode-hook)

(defun ediff-copy-both-A-and-B-to-C ()
  (interactive)
  (ediff-copy-diff ediff-current-difference nil 'C nil
                   (concat
                    (ediff-get-region-contents ediff-current-difference 'A ediff-control-buffer)
                    (ediff-get-region-contents ediff-current-difference 'B ediff-control-buffer))))
(defun add-d-to-ediff-mode-map () (define-key ediff-mode-map "B" 'ediff-copy-both-A-and-B-to-C))
(add-hook 'ediff-keymap-setup-hook 'add-d-to-ediff-mode-map)

(defun calc-eval-region (arg beg end)
  "Calculate the region and display the result in the echo area.
With C-u, insert the result at the end of region.
With C-u C-u, replace region with the result"
  (interactive "P\nr")
  (let* ((expr (buffer-substring-no-properties beg end))
         (result (calc-eval expr)))
    (cond
     ((equal arg nil) ; no C-u
      (message "%s = %s" expr result))
     ((equal arg '(4)) ;; C-u
      ;; Print result next to region
      (goto-char end)
      (save-excursion
        (insert result)))
     ((equal arg '(16)) ; C-u 2
      ;; Replaces the region with result
      (delete-region beg end)
      (goto-char beg)
      (save-excursion
        (insert result)))

     (t ; all other cases, prompt
      (message "Too much universal arg: %s" arg))
     )))

(defun dumb-extract-variable (begin end var)
  (interactive "r\nsVariable name: ")
  (kill-region begin end)
  (insert var)
  (move-end-of-line 0)
  (newline-and-indent)
  (insert var " = ")
  (yank)
  )

(defun dumb-inline-variable ()
  (interactive)
  (let ((var (current-word)))
    (let ((value (buffer-substring (point) (point-at-eol))))
      (kill-whole-line)
      (search-forward var)
      (replace-match value))))

(defun dumb-extract-function (begin end var)
  (interactive "r\nsfunction name: ")
  (kill-region begin end)
  (insert var "()")
  (newline-and-indent)
  (move-end-of-line 0)
  (newline-and-indent)
  (insert "func " var "():\n")
  (yank))

(defun dumb-inline-function ()
  (interactive)
  (let ((var (current-word)))
    (let ((value (buffer-substring (point) (point-at-eol))))
      (kill-whole-line)
      (search-forward var)
      (search-forward "):\n")
      (replace-match value))))

(map!
  (:map compilation-mode :nv "h" #'evil-backward-char))

(fset 'macro-run-3-godot-debug-setup-fish-game
   (kmacro-lambda-form [?a up escape ?  ?w ?w ?a up escape ?  ?w ?w ?a up escape return return escape
                        ?  ?w ?w ?a return escape
                        ?  ?w ?w ?a return escape] 0 "%d"))

(add-to-list 'evil-ex-commands
             '("n" . "normal"))

(defhydra hydra-expand-region ()
  "region: "
  ("k" er/expand-region "expand")
  ("j" er/contract-region "contract"))

(evil-define-key 'visual 'global (kbd "v") #'hydra-expand-region/body)

(setq vimish-fold-marks '("//region" . "//endregion"))

(after! evil-traces
  (evil-traces-use-diff-faces) ; if you want to use diff's faces
  (evil-traces-mode))

(setq gdscript-gdformat-line-length 80)
