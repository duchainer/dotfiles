;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el

;; To install a package with Doom you must declare them here, run 'doom sync' on
;; the command line, then restart Emacs for the changes to take effect.
;; Alternatively, use M-x doom/reload.


;; Doom's packages are pinned to a specific commit and updated from release to
;; release. The `unpin!' macro allows you to unpin single packages...
;(unpin! pinned-package)
;; ...or multiple packages
;(unpin! pinned-package another-pinned-package)
;; ...Or *all* packages (NOT RECOMMENDED; will likely break things)
;(unpin! t)


;; To install SOME-PACKAGE from MELPA, ELPA or emacsmirror:
;(package! some-package)
(package! org-alert)

;; To install a package directly from a particular repo, you'll need to specify
;; a `:recipe'. You'll find documentation on what `:recipe' accepts here:
;; https://github.com/raxod502/straight.el#the-recipe-format
;(package! another-package
;  :recipe (:host github :repo "username/repo"))

;; If the package you are trying to install does not contain a PACKAGENAME.el
;; file, or is located in a subdirectory of the repo, you'll need to specify
;; `:files' in the `:recipe':
;(package! this-package
;  :recipe (:host github :repo "username/repo"
;           :files ("some-file.el" "src/lisp/*.el")))

;; If you'd like to disable a package included with Doom, for whatever reason,
;; you can do so here with the `:disable' property:
;(package! builtin-package :disable t)

;; You can override the recipe of a built in package without having to specify
;; all the properties for `:recipe'. These will inherit the rest of its recipe
;; from Doom or MELPA/ELPA/Emacsmirror:
;(package! builtin-package :recipe (:nonrecursive t))
;(package! builtin-package-2 :recipe (:repo "myfork/package"))

;; Specify a `:branch' to install a package from a particular branch or tag.
;; This is required for some packages whose default branch isn't 'master' (which
;; our package manager can't deal with; see raxod502/straight.el#279)
;(package! builtin-package :recipe (:branch "develop"))

;; "diff" two directories
;; see https://github.com/fourier/ztree
(package! ztree)

;; Rust execution for code blocks in org mode
(package! ob-rust)

;; Edit godot-script in emacs
;; (package! gdscript-mode
;;           :recipe (:host github
;;                    :repo "duchainer/emacs-gdscript-mode"))

;; Track Emacs commands frequency
;; see https://github.com/dacap/keyfreq
(package! keyfreq)

;; C++ auto formating
(package! clang-format)

;; Write LaTex using the emacs calculator
;; see https://github.com/johnbcoughlin/calctex
(package! calctex
  :recipe (:host github :repo "johnbcoughlin/calctex"
             :branch "master"))
;;;
;;;
;;; EDITOR
;;;
;;;

;; For autocomplete from same file
(package! auto-complete)

;; To edit browser text area using Emacs
;; Allow to use GhostText for live bi-directional editing
(package! atomic-chrome)

;;;
;;; EVIL
;;;

;; Repeatable replace <motion> with register content
;; see https://github.com/Dewdrops/evil-ReplaceWithRegister
;; TODO allow using C-p for paste cycling
(package! evil-replace-with-register)

;; Cycle text objects through camelCase, kebab-case, snake_case and UPPER_CASE.
;; see https://github.com/ninrod/evil-string-inflection
;; Use g~ as operator
(package! evil-string-inflection)

;; Preview ex commands
;; See https://github.com/mamapanda/evil-traces
(package!  evil-traces)


;; Add some ex commands
;; See https://github.com/edkolev/evil-expat
(package! evil-expat)

;; Just a table to correspond ascii chars
;; to (decimal, octal or hexadecimal) numerals
(package! ascii-table)

;; Machine-learning for better auto-completion
;; (package! company-tabnine)



;; Nesting-sensitive search-and-replace
;; see https://github.com/s-kostyaev/comby.el
(package! comby)

;; Services management in emacs
;; click on columns to sort, for example by enable
(package! daemons)

;;;
;;; DIRED
;;;

;; Preview files in dired
;; TODO Find why it needs to be toggled off and on to preview the next file
(package! peep-dired)

;; (package! dired-hacks)



;;;
;;; FUN
;;;

;; Echo a new dad-joke in mini-buffer
(package! dad-joke)
