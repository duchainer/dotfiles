#!/usr/bin/env bash
# Adapted from https://euandre.org/til/2020/08/28/grep-online-repositories.html 
set -euo pipefail
# set -eu

GIT_SEARCH_FOLDER="~/Documents/tmp_lite/git-search"

end="\033[0m"
red="\033[0;31m"
red() { echo -e "${red}${1}${end}"; }

usage() {
  red "Missing argument $1.\n"
  cat <<EOF
Usage:
    $0 <REGEX_PATTERN> <REPOSITORY_URL>

      Arguments:
        REGEX_PATTERN     Regular expression that "git grep" can search
        REPOSITORY_URL    URL address that "git clone" can download the repository from

Examples:
    Searching "make get-git" in cgit repository:
        git search 'make get-git' https://git.zx2c4.com/cgit/
        git search 'make get-git' https://git.zx2c4.com/cgit/ -- \$(git rev-list --all)
EOF
  exit 2
}


REGEX_PATTERN="${1:-}"
REPOSITORY_URL="${2:-}"
[[ -z "${REGEX_PATTERN}" ]] && usage 'REGEX_PATTERN'
[[ -z "${REPOSITORY_URL}" ]] && usage 'REPOSITORY_URL'

mkdir -p  ${GIT_SEARCH_FOLDER}
DIRNAME="$(echo "${REPOSITORY_URL%/}" | rev | cut -d/ -f1 | rev)"
if [[ ! -d "${GIT_SEARCH_FOLDER}/${DIRNAME}" ]]; then
  git clone "${REPOSITORY_URL}" "${GIT_SEARCH_FOLDER}/${DIRNAME}"# "/tmp/git-search/${DIRNAME}"
fi
pushd "${GIT_SEARCH_FOLDER}/${DIRNAME}"

shift 3 || shift 2 # when "--" is missing
# Perform git grep, forwarding the remaining arguments from $@.
git grep "${REGEX_PATTERN}" "${@}"

