#/bin/sh
# May need to run the three next line before, if you need a newer emacs
# sudo add-apt-repository ppa:kelleyk/emacsi
# sudo apt-get update
# sudo apt install emacs26

git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs-doom.d
ln -s ~/.emacs-doom.d ~/.emacs.d
~/.emacs.d/bin/doom install
~/.emacs.d/bin/doom sync
