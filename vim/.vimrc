" For now this is as basic as it gets (not using vim much since I started using Doom Emacs instead)

" Always wrap long lines:
set wrap
" Always show line numbers
set number
"Expand undo stack
set history=600

" Ignore case when searching
set ignorecase

" Magic for RegEx
set magic

" What else?
set encoding=utf8

" Use spaces instead of tabs
set expandtab

" Be smart when using tabs ;)
set smarttab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

" Set autoindent
set ai
set si

" Set syntax (color)
syntax on
