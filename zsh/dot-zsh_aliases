# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
	test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

#
#CUSTOM ALIASES
#
alias ...='../..'
alias ....='../../..'

#git
alias g='git'
alias gch='git checkout'
alias gco='git commit'
alias gcoa='git commit --amend'
alias gl='git log --name-status'
alias gpu='git push'
alias gpl='git pull'
alias gr='git reset'
alias gr1='git reset HEAD~1'
alias grb='git rebase'
alias gs='git status'
alias GPL='for D in *; do
    if [ -d "${D}" ]; then
        echo -e "\033[4;37m${D}\e[0m";
        cd "${D}";
        git checkout master| grep -v "Already on 'master'";
        git pull;
        cd ..;
    fi
done'

alias SOURCERC="source ~/.zshrc"
alias EDITRC="e ~/.zshrc && SOURCERC"
alias EDIT_ALIASES='e ~/.zsh_aliases && SOURCERC;'

#
# Quality of Life
#
alias APT_INSTALL="sudo apt install"

#Runs doom emacs manager
alias EMACS_DOOM=~/.emacs.d/bin/doom

#Allow updating rust-analyzer "seemlessly"
alias UPDATE_RUST_ANALYZER="
    pushd /tmp; # Go to /tmp while storing starting dir
    git clone https://github.com/rust-analyzer/rust-analyzer.git &&
    pushd rust-analyzer; # Go into rust-analyzer/ while storing /tmp
    cargo xtask install --server;
    popd; # Return to /tmp
    popd; # Return to your starting dir
"
alias e="emacsclient -a ''"
alias gcl='gitlab-ci-local'

alias android-studio="~/Documents/Documents_240gb/programs/android-studio/bin/studio.sh"

# Glamorous toolkit
alias GTOOLKIT_UPDATE='wd gtoolkit; curl https://dl.feenk.com/scripts/linux.sh | bash ; mv glamoroustoolkit "$(date -u +"%Y-%m-%d")"; ln -sfTv "$(date -u +"%Y-%m-%d")" latest_version'
alias GTOOLKIT_RUN='wd gtoolkit; ./latest_version/bin/GlamorousToolkit'

# Open full Godot Hanzo project
alias HANZO='wd upwork_projects && bash open_godot_hanzo_project.sh'

alias SSH_CED='cd ~/.ssh; ln -sf id_ced_ed25519 id_ed25519; ln -sf id_ced_ed25519.pub id_ed25519.pub'
alias SSH_RAPH='cd ~/.ssh; ln -sf id_raph_ed25519 id_ed25519; ln -sf id_raph_ed25519.pub id_ed25519.pub'
alias the_mirror=~/Documents/programs/the-mirror/godot-voxel.x11.tools.64
